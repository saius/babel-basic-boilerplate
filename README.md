Este repo es para implementar las buenas prácticas de [Babel](https://babeljs.io/) siguiendo
el [tutorial oficial](https://babeljs.io/docs/en/).

Se usan las siguientes librerías:

- [@babel/core](https://babeljs.io/docs/en/babel-core/) Babel
/ [config](https://babeljs.io/docs/en/configuration)
- [@babel/preset-env](https://babeljs.io/docs/en/babel-preset-env) soporte básico para JS moderno
/ [targets config](https://github.com/browserslist/browserslist#full-list)
/ [useBuiltIns config](https://babeljs.io/docs/en/babel-preset-env#usebuiltins)
- [@babel/plugin-transform-runtime](https://babeljs.io/docs/en/babel-plugin-transform-runtime/) para optimización del código
/ [plugins config](https://babeljs.io/docs/en/babel-plugin-transform-runtime/#usage)
/ [corejs config](https://babeljs.io/docs/en/babel-plugin-transform-runtime/#corejs)
/ [@babel/runtime-corejs3](https://babeljs.io/docs/en/babel-runtime-corejs2)
- [core-js](https://github.com/zloirock/core-js#babel) para polyfills de @babel/preset-env
- [babel/cli](https://babeljs.io/docs/en/babel-cli) el cli para compilar
Para crear el proyecto hacer:
```
npm init -y
npm i -D @babel/core@7 @babel/preset-env@7 @babel/plugin-transform-runtime@7
npm i core-js@3.8 @babel/runtime-corejs3@7
```

Para testear la compilación con Babel:
```
npm i -D @babel/cli
npx babel src --out-dir lib
```
y mirar output en `lib/test.js`. También se puede agregar `-w` para watch-ear.

Para comprobar que ejecuta bien hacer `node lib/test.js`

_Nota: El archivo `babel.config.js.example` es un ejemplo de configuración dinámica usando la [Configuration API](https://babeljs.io/docs/en/config-files#config-function-api). Para probarlo sacarle el `.example`, renombrar temporalmente el archivo de configuración viejo y agregar `NODE_ENV=production` al principo del comando `npx babel`._
