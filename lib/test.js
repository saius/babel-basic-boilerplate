"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

require("core-js/modules/es.object.to-string");

require("core-js/modules/es.promise");

require("core-js/modules/es.promise.finally");

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _includes = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/includes"));

var _assign = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/object/assign"));

var _promise = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/promise"));

// probar arrow function
var c = function c() {
  return true;
}; // probar Promise


var b = new _promise.default(c); // probar finally

b.finally(); // probar Object.assign

(0, _assign.default)({}, {
  a: b
}); // probar Array.includes

b = [2, 4, 6];
(0, _includes.default)(b).call(b, 4); // probar class

var A = function A() {
  (0, _classCallCheck2.default)(this, A);
}; // este comentario debería quedarse al minificar:
// GPL License bla bla