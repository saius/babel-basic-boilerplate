// Ejemplo de config dinámica usando Configuration API
// https://babeljs.io/docs/en/config-files#config-function-api

module.exports = function(api) {
  // Mirar valor de NODE_ENV
  // https://babeljs.io/docs/en/config-files#apienv
  const developmentMode = api.env("development")

  // Cachear config a menos que cambie NODE_ENV
  // https://babeljs.io/docs/en/config-files#apicache
  api.cache.invalidate(() => developmentMode);

  const presets = [
    [
      "@babel/env",
      {
        "targets": "> .25% in AR, not dead",
        "useBuiltIns": "usage",
        "corejs": 3.8
      }
    ]
  ];
  const plugins = [
    ["@babel/plugin-transform-runtime", {
      "corejs": 3
    }]
  ];

  return {
    presets,
    plugins,
    minified: !developmentMode,
    // https://babeljs.io/docs/en/options#comments
    shouldPrintComment: developmentMode ? undefined : (val => /license|@preserve/i.test(val))
  };
}
