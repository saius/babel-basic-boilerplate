// probar arrow function
var c = () => true

// probar Promise
let b = new Promise(c)

// probar finally
b.finally()

// probar Object.assign
Object.assign({}, {
  a: b
})

// probar Array.includes
b = [2, 4, 6]
b.includes(4)

// probar class
class A {}

// este comentario debería quedarse al minificar:
// GPL License bla bla